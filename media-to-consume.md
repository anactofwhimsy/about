## Movies
- [x] The Menu (Ashish R)
- [ ] Leon The Professional (Riccardo T)
- [ ] Midsommar
- [ ] Operation Fortune (Mike T)
- [x] M3gan (Mike T)
- [ ] Beau is Afraid (Mike T)
- [ ] Nightmare Alley
- [ ] The Act of Killing
- [ ] Eddie the eagle

## TV Shows
- [x] Ted Lasso
- [x] Abbot Elementary

## Comics
- [ ] Transmepolitan (Charles)
- [ ] Persepholis (Charles)
- [ ] Planetary (Charles)
- [ ] The Authority (Charles)

## Books
- [ ] The Man In The High Castle
- [ ] Extreme Privacy: What it takes to Disappear
- [ ] Night Flyers
- [ ] Whole Numbers and Half Truths
- [ ] Putins Kleptocracy
- [ ] Daughters of Erebus
- [ ] City of Thieves
- [ ] The Memory Librarian: And Other Stories of Dirty Computer
- [x] Discworld (Alvin G and Mike)
- [x] Nation (Alvin G)
- [x] Amusing Ourselves to Death (John G)
- [ ] How to keep house while drowning
- [x] Tower of Babylon
- [ ] three body problem (Matthew B)
## Games
- [ ] Terrible Candidates (Mike T)
- [ ] Catapult Feud (Mike T)

