# About
Hello! I am a human living in Aotearoa with my best friend of 15 years, Phoebe Weiners - number one spy. 

## Badges
I have no badges, but I did eat a lot of girlscout cookies from 1994 - 2012

## Visuals
(┛◉Д◉) ┛彡┻━┻ 
¯\_(ツ)_/¯ 

## Installation
I integrate well with neurodivergent people and people who went through traumatic events in their childhood

## Usage
Pretty okay human for laughs. Can hike, bike, paraglide, and climb decent enough. Will eat your cooking. 

## Support
Everyone needs therapy. Please seek professional help. I am not a licenced therapist and any advice I would give you is just an opinion

## Roadmap
Die before retirment age

## Contributing
Support me on Patreon. Also remind me to sign up for Patreon.

## Authors and acknowledgment
Thanks to my mother for giving birth to me and contributing to my early development trauma. I wouldn't be where I am without it!*

## License
* NZ Driver Licence Class 1
* Paraglider Level 2
* ARO Rating (I can listen to the airport. Yippee)

## Project status
Your girl is in therapy, Hell yeah!

\* No shade to my mom (well I guess it's a little shady), but I really am happy with where I am in life and I wouldn't be here if not for the hardships.  
