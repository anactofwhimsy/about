## Movies
- Spider-Man: Into the Spider-verse 
- Spider-Man: Across the Spider-verse
- Mad max Fury Road
- Hot fuzz (any Edgar wright film really. Except I didn't care for The World's End)
- Annihilation
- Pans labyrinth
- Singin' in the rain
- Get out
- Emperors new groove
- Both Incredibles filmds
- Booksmart
- Ex machina 
- Everything Everywhere All At Once

## Bad movies
- Troll 2
- Resident evil series
- [Voyage Of the Rock Aliens](https://www.youtube.com/watch?v=csgGEZzhojA)

## TV shows
- Arcane
- Schitts creek
- Parks and rec
- Battle Star Galactica 
- The Expanse
- First season of west world. Proceed no further 
- Pushing daises
- What We Do in The Shadows
- Harley Quinn 
- Drag race
- Love craft country
- Avatar: TLA and LoK 
- Crazy ex girlfriend
- Hey Arnold 
- Seinfeld
- Arrested development 
- Cunk on earth
- Fleabag
- Broadchurch
- Ted Lasso

## Documentaries
- Wild wild country
- Icarus
- How to die in Oregon 
- Jodoroskys dune
 
## Books
- Fahrenheit 451
- Do androids dream of electric sheep 
- Life of pi
- American gods
- World war z
- David sedaris 
- Ray Bradbury 
- Neil gaiman 
- Annihilation
- Scenes of my life Michael k Williams 
- Tower of Babel series by Josiah Bancroft

## Comics
- Saga
- Bone
- Y the last man
- Invincible
- Poorly drawn lines
- Xkcd
- Hyperbole and a half
- Strange planet
- Sarah anderson

## Podcasts
- Best friends with sasheer and Nicole 
- Darknet diaries
- How did this get made?
- Scam Goddess
- Unspooled
- My dad wrote a porno
- Shittown 

## Video games
- The last of us Part 1 and 2
- Mass Effect
- Burnout (not paradise)
- Pokémon (blue and silver)
- Bioshock infinite
- Psychonauts 
- Jack box party games
- Rollercoaster tycoon
- Beat Saber
- Horizon (even if I still haven't beat it)
- Spider-man

## Board games
- Dominion
- Lost ruins of arnak
- Wingspan
- Photosynthesis
- Splendor
- Hanabi 
- Gloomhaven
- Dixit

# Music
- St Vincent 
- Glitch mob
- Janelle Monaé 
- Metric
- Yeah yeah yeahs
- Pink Floyd 
- Phoebe bridges
- The killers
- Pendulum
- Florence and the machine
- Gorillaz
- Ben folds
- Queen
- Rammstein 
- Marina
- Death Cab For Cutie
- Kendrick Lamar
- Childish Gambino
- Mashrou' Leila

# Things I like
- Improv 
- Comedy
- Paragliding
- Animals
- Puzzles
- Pottery
- Gardening
- Learning
- Snowboarding
- Biking
- Hiking
- Fixing things
- Climbing
- Rain
- Sony wh-1000mx headphones
- Thunder pants
- Darn tough socks
- You (teeheehee)

## Non profits
- Friends of the children 
- Oregon humane society
- Aspca
- Austin humane society
- Bat world
- Xerces society
- Underdog rescue Moab 
- Ihc New Zealand
- Dunedin wildlife hospital 
- One Bike

## Food

- All cane fruits, especially raspberries
- Peanut butter
- Pecans
- Costello blue cheese
- Croissant with tomato, brie, and eggs
- Salmon sushi
- The spagetti bolognase from Giovi 3 years ago

## Thing I don’t like
- Pho / most Vietnamese (food, not the people, they're fine)
- Vegetables of the following variety
 - Cilantro (no, it doesn't taste like soap, it just tastes bad)
 - Celery
 - Cucumber
- Karaoke
- Loud bars
- Paintball
- Food that falls apart in you hands like a bad burger or poorly wrapped burrito
- Billionares 
- People that just want to argue
- Furiosa (seriously, every damn man from Fury Road could reprise their role, but Charlize was too old. Yeah, okay Miller, fuck off with this shitty CGI)
